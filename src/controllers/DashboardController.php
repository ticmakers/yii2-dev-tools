<?php

namespace ticmakers\tools\controllers;

use Yii;
use ticmakers\core\base\Controller;

/**
 * Controlador DashboardController
 *
 * @package ticmakers\tools\controllers\DashboardController
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S. 
 * @version 0.0.1
 * @since 1.0.0
 */
class DashboardController extends Controller
{

    public function actionUpdateSecuences()
    {
        Yii::$app->dbHelper->updateSequences();
        return $this->redirect(['index']);
    }
}
