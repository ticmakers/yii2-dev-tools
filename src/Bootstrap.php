<?php

namespace ticmakers\tools;

use yii\base\Application;
use ticmakers\core\base\Bootstrap as BootstrapBase;
use Yii;

/**
 * Clase cargadora de características para la aplicación.
 *
 * @package ticmakers\tools\Booststrap
 * 
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @version 0.0.1
 * @since 0.0.0
 */
class Bootstrap extends BootstrapBase
{
    public $moduleId = 'tools';
    public $pieces = [
        'modules' => [
            'tools' => \ticmakers\tools\Module::class
        ],
        'components' => [
            'dbHelper' => \ticmakers\tools\helpers\DbToolsHelper::class,
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        parent::bootstrap($app);

        Yii::$app->getAssetManager()->publish('@ticmakers/tools/assets');

        if ($app instanceof \yii\console\Application) {
            $app->getModule($this->moduleId)->controllerNamespace =
                'ticmakers\tools\commands';
        }
    }
}
